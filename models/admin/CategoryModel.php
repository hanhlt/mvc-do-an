<?php
    include 'models/config.php';
    class CategoryModel
    {
        var $db;

        public function __construct()
        {
            $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        }

        public function getAllCategory()
        {
            $sql = "SELECT * FROM `category` ";
            $data = $this->db->query($sql);
            return $data;
        }

        public function getCategory($id)
        {
            $sql = "SELECT * FROM `category` WHERE `id` = '$id'";
            $data = $this->db->query($sql);
            return $data;
        }

        public function create($name)
        {
            $slug = strtolower(convert_name($name));
            $sql = "INSERT INTO `category`(`name`, `slug`) VALUES ('$name', '$slug')";

            if($this->db->query($sql))
                return true;
            return false;
        }

        public function update($name, $id)
        {
            $slug = strtolower(convert_name($name));
            $sql = "UPDATE `category` SET `name`= '$name',`slug`= '$slug' WHERE `id`= '$id'";
            if($this->db->query($sql))
                return true;
            return false;
        }
        public function delete( $id)
        {
            $sql = "DELETE FROM `category` WHERE `id` = '$id'";
            if($this->db->query($sql))
                return true;
            return false;
        }
    }
