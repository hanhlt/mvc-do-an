<?php
    include 'models/config.php';
    class FoodModel
    {
        var $db;

        public function __construct()
        {
            $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        }

        public function getAllCategory()
        {
            $sql = "SELECT * FROM `category`";
            $data = $this->db->query($sql);
            return $data;
        }

        public function create($name, $price, $sale, $category_id, $avatar)
        {
            $sql = "INSERT INTO `food`(`name`, `category_id`, `price`, `sale`, `avatar`) VALUES ('$name', '$category_id','$price','$sale', '$avatar')";
            if($this->db->query($sql))
                return true;
            return false;
        }

        public function getAllFood()
        {
            $sql = "SELECT * FROM `food`ORDER BY id DESC;";
            $data = $this->db->query($sql);
            return $data;
        }

        public function getFood($id)
        {
            $sql = "SELECT * FROM `food` WHERE id = '$id'";
            $data = $this->db->query($sql);
            return $data;
        }

        public function update($id, $name, $category_id, $price, $sale, $avatar)
        {
            $sql = "UPDATE `food` SET `name`='$name',`category_id`='$category_id',`price`='$price',`sale`='$sale',`avatar`='$avatar' WHERE `id`= '$id'";

            if($this->db->query($sql))
                return true;
            return false;
        }

        public function delete( $id)
        {
            $sql = "DELETE FROM `food` WHERE `id` = '$id'";
            if($this->db->query($sql))
                return true;
            return false;
        }

        public function getUrlAvtar($id)
        {
            $sql = "SELECT * FROM `food` WHERE `id` = '$id'";
            $data = $this->db->query($sql);;

            while ($res = $data->fetch_assoc()){
                $avatar = $res['avatar'];
            }

            return $avatar;
        }
    }
