<?php
    include 'models/admin/CategoryModel.php';
    include 'views/admin/CategoryView.php';
    include 'controllers/function.php';

    class DanhMucController
    {
        var $model;
        var $view;

        public function __construct()
        {
            $this->model = new CategoryModel();
            $this->view = new CategoryView();
        }

        public function home()
        {
            $data = $this->model->getAllCategory();
            $this->view->home($data);
        }

        public function add()
        {
            $this->view->add();
        }
        public function create()
        {
            $name = $_POST['name'];
            $result = $this->model->create($name);
            if($result===true){
                header("Location: /admin/danhmuc/home");
            }else{
                header("Location: /admin/danhmuc/add");
            }
        }

        public function edit()
        {
            $id = $_GET['id'];
            $data = $this->model->getCategory($id);
            $this->view->edit($data);
        }

        public function update()
        {
            $id = $_POST['id'];
            $name = $_POST['name'];
            $result = $this->model->update($name, $id);
            if($result===true){
                header("Location: /admin/danhmuc/home");
            }else{
                header("Location: /admin/danhmuc/edit?id=".$id);
            }
        }
        public function delete()
        {
            $id = $_GET['id'];
            $result = $this->model->delete($id);
            if($result===true){
                header("Location: /admin/danhmuc/home");
            }else{

            }
        }
    }
