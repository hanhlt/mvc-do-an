<?php
    include 'models/admin/FoodModel.php';
    include 'views/admin/FoodView.php';
    include 'controllers/function.php';

    class FoodController
    {
        var $model;
        var $view;

        public function __construct()
        {
            $this->model = new FoodModel();
            $this->view = new FoodView();
        }

        public function home()
        {
            $foods = $this->model->getAllFood();
            $this->view->home($foods);
        }

        public function add()
        {
            $categories = $this->model->getAllCategory();
            $this->view->add($categories);
        }

        public function create()
        {
            $name = $_POST['name'];
            $price = $_POST['price'];
            $sale = $_POST['sale'];
            $category_id = $_POST['category_id'];

            $path = "data/";
            $tmp_name = $_FILES['avatar']['tmp_name'];
            $avatar = $_FILES['avatar']['name'];
            move_uploaded_file($tmp_name,$path.$avatar);

            $result = $this->model->create($name, $price, $sale, $category_id, $path.$avatar);

            if($result===true){
                header("Location: /admin/food/home");
            }else{
                header("Location: /admin/food/add");
            }

        }

        public function edit()
        {
            $id = $_GET['id'];
            $food = $this->model->getFood($id);
            $categories = $this->model->getAllCategory();

            $this->view->edit($food, $categories);
        }

        public function update()
        {
            $id = $_POST['id'];
            $name = $_POST['name'];
            $price = $_POST['price'];
            $sale = $_POST['sale'];
            $category_id = $_POST['category_id'];


            $avatar = $this->model->getUrlAvtar($id);

            if(!empty($_FILES['avatar']['tmp_name'])){
                $path = "data/";
                $tmp_name = $_FILES['avatar']['tmp_name'];
                $avatar = $_FILES['avatar']['name'];

                move_uploaded_file($tmp_name,$path.$avatar);
            }

            $result = $this->model->update($id, $name, $category_id, $price, $sale, $path.$avatar);

            if($result===true){
                header("Location: /admin/food/home");
            }else{
                header("Location: /admin/food/edit?id=".$id);
            }
        }

        public function delete()
        {
            $id = $_GET['id'];
            $result = $this->model->delete($id);
            if($result===true){
                header("Location: /admin/food/home");
            }else{

            }
        }
    }
