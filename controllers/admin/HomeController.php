<?php
    include 'views/admin/HomeView.php';

    class HomeController
    {
        var $view;

        public function __construct()
        {
            $this->view = new HomeView();
        }

        public function index()
        {
            $this->view->index();
        }
    }
