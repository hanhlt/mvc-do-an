<?php
    include 'models/admin/NewModel.php';
    include 'views/admin/NewView.php';
    include 'controllers/function.php';

    class NewController
    {
        var $model;
        var $view;

        public function __construct()
        {
            $this->model = new NewModel();
            $this->view = new NewView();
        }

        public function home()
        {
            $this->view->home();
        }

        public function add()
        {
            $this->view->add();
        }

        public function create()
        {
            $title = $_POST['title'];
            $content =  $_POST['content'];
            $slug = $slug = strtolower(convert_name($title));
            $date = date("d-m-Y", strtotime(date("Y/m/d")));

            $path = "data/";
            $tmp_name = $_FILES['avatar']['tmp_name'];
            $avatar = $_FILES['avatar']['name'];
            move_uploaded_file($tmp_name,$path.$avatar);

            $result = $this->model->create($title, $content, $slug, $path.$avatar, $date);

            if($result===true){
                header("Location: /admin/new/home");
            }else{
                header("Location: /admin/new/add");
            }
        }

        public function edit()
        {
            $id = $_GET['id'];
            $data = $this->model->getNew($id);
            $this->view->edit($data);
        }

        public function update()
        {
            $id = $_POST['id'];
            $title = $_POST['title'];
            $content =  $_POST['content'];
            $slug = $slug = strtolower(convert_name($title));
            $date = date("d-m-Y", strtotime(date("Y/m/d")));

            $avatar = $this->model->getUrlAvtar($id);

            if(!empty($_FILES['avatar']['tmp_name'])){
                $path = "data/";
                $tmp_name = $_FILES['avatar']['tmp_name'];
                $avatar = $_FILES['avatar']['name'];

                move_uploaded_file($tmp_name,$path.$avatar);
            }

            $result = $this->model->update($title, $content, $slug, $path.$avatar, $date, $id);

            if($result===true){
                header("Location: /admin/new/home");
            }else{
                header("Location: /admin/new/edit?id=".$id);
            }
        }
    }
