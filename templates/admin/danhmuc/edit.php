<?php include 'templates/admin/layout/header.php'; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include 'templates/admin/layout/nav.php'; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row mt-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin/home/danhmuc">Home</a></li>
                                <li class="breadcrumb-item"><a href="/admin/danhmuc/home">Danh mục</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Cập nhật</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <form class="form-group col-5" action="/admin/danhmuc/update" method="post">
                            <div class="form-group">
                                <label>Tên danh mục</label>
                                <?php while($row = $data->fetch_assoc()) {?>
                                    <input name="id" value="<?=$row['id']?>" type="hidden">
                                    <input type="text" name="name" value="<?=$row['name']?>" class="form-control" required>
                                <?php }; ?>
                            </div>
                            <p>
                                <a href="/admin/danhmuc/home" class="btn btn-danger">Trở lại</a>
                                <input type="submit" name="btn" value="Cập nhật" class="btn btn-success">
                            </p>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>
<?php include 'templates/admin/layout/footer.php'; ?>