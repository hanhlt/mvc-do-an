<?php include 'templates/admin/layout/header.php'; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include 'templates/admin/layout/nav.php'; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row mt-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin/home/danhmuc">Home</a></li>
                                <li class="breadcrumb-item"><a href="/admin/danhmuc/home">Danh mục</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <form class="form-group col-5" action="/admin/danhmuc/create" method="post">
                            <div class="form-group">
                                <label>Tên danh mục</label>
                                <input type="text" name="name" placeholder="Tên danh mục" class="form-control" required>
                            </div>
                            <p>
                                <a href="/admin/danhmuc/home" class="btn btn-danger">Trở lại</a>
                                <input type="submit" name="btn" value="Thêm" class="btn btn-success">
                            </p>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>
<?php include 'templates/admin/layout/footer.php'; ?>