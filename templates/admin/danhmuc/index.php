<?php include 'templates/admin/layout/header.php'; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include 'templates/admin/layout/nav.php'; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row mt-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin/home/index">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Danh mục</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <a href="/admin/danhmuc/add" class="btn btn-success  mb-3">Thêm mới</a>
                        <table class="table text-center">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Tên danh mục</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php while($row = $data->fetch_assoc()) {?>
                                <tr>
                                    <td><?=$row['id']?></td>
                                    <td><?=$row['name']?></td>
                                    <td>
                                        <a href="/admin/danhmuc/edit?id=<?=$row['id']?>" class="btn btn-success">Cập nhật</a>
                                        <a href="/admin/danhmuc/delete?id=<?=$row['id']?>" class="btn btn-danger">Xoá bỏ</a>
                                    <td>
                                </tr>
                            <?php }; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
        </div>
    </div>
<?php include 'templates/admin/layout/footer.php'; ?>