<?php include 'templates/admin/layout/header.php'; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include 'templates/admin/layout/nav.php'; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row mt-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin/home/danhmuc">Home</a></li>
                                <li class="breadcrumb-item"><a href="/admin/new/home">Bài viết</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <form class="form-group col-8" action="/admin/new/create" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title">Tiêu đề<span class="text-danger">(*)</span></label>
                                <input type="text" name="title" placeholder="Tiêu đề bài viết" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="content">Nội dung<span class="text-danger">(*)</span></label>
                                <textarea class="form-control" rows="10" name="content" id="content"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="avatar">Hình ảnh</label>
                                <input type="file" class="form-control-file" name="avatar">
                            </div>
                            <p>
                                <a href="/admin/new/home" class="btn btn-danger">Trở lại</a>
                                <input type="submit" name="btn" value="Thêm" class="btn btn-success">
                            </p>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script>
        ClassicEditor
            .create( document.querySelector( '#content' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
<?php include 'templates/admin/layout/footer.php'; ?>