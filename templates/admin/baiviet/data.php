<?php
    include_once '../../../models/config.php';
    $dong = 4;
    $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if($db->connect_errno){
        die("Eror");
    }
    $sotrang = filter_var($_POST['page'], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($sotrang)){
        header("HTTP/1.1 500 Invalid page number!");
        exit();
    }
    $vitri = (($sotrang-1) * $dong);
    $lthanh = $db->prepare("SELECT id, title, avatar, date FROM `news` LIMIT ?, ?");
    $lthanh->bind_param("dd", $vitri, $dong);
    $lthanh->execute();
    $lthanh->bind_result($id, $title, $avatar, $date);
    while ($lthanh->fetch()){
        echo '<tr>';
        echo '<td  class="align-middle">'.$id.'</td>';
        echo '<td  class="align-middle"><img src="/'.$avatar.'" style="width: 100px; height: 80px;"></td>';
        echo '<td  class="align-middle">'.$title.'</td>';
        echo '<td  class="align-middle">'.$date.' </td>';
        echo '<td  class="align-middle">
                   <a href="/admin/new/edit?id='.$id.'" class="btn btn-success">Cập nhật</a>
                   <a href="/admin/new/delete?id='.$id.'" class="btn btn-danger" onclick="confirmDelete(this)">Xoá bỏ</a>
              </td>';
        echo '</tr>';
    }
?>