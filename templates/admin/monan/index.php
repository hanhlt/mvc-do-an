<?php include 'templates/admin/layout/header.php'; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include 'templates/admin/layout/nav.php'; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row mt-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin/home/index">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Món ăn</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <a href="/admin/food/add" class="btn btn-success  mb-3">Thêm mới</a>
                        <table class="table text-center">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Tên món</th>
                                    <th>Ảnh</th>
                                    <th>Giá</th>
                                    <th>Giảm giá</th>
                                    <th>Thao thác</th>
                                </tr>
                            </thead>
                            <tbody class="data-food">
                            </tbody>
                        </table>
                        <div class="row col-sm-12 mb-5">
                            <button class="btn btn-success" id="load-data" style="margin: auto;font-size: 15px;"><img class="img_load" src="../../data/load.gif" class="ani_image" style="width: 20px"> Xem thêm</button>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript">
        var mypage = 1;
        mycontent(mypage);
        $('#load-data').click(function (e) {
            mypage++;
            mycontent(mypage);
        })
        function  mycontent(mypage) {
            $('.img_load').show()
            $.post('/templates/admin/monan/food.php', {page:mypage}, function (data) {
                if(data.trim().length == 0){
                    $('#load-data').text("Đã tải hết dữ liệu").prop("disabled", true);
                }
                $('.data-food').append(data);
                $("html, body").animate({scrollTop:$("#load-data").offset().top}, 800)
                $('.img_load').hide()
            })
        }
    </script>
<?php include 'templates/admin/layout/footer.php'; ?>