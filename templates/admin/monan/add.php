<?php include 'templates/admin/layout/header.php'; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include 'templates/admin/layout/nav.php'; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row mt-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin/home/danhmuc">Home</a></li>
                                <li class="breadcrumb-item"><a href="/admin/food/home">Món ăn</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <form class="form-group col-5" action="/admin/food/create" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name">Tên món ăn <span class="text-danger">(*)</span></label>
                                <input type="text" name="name" placeholder="Tên món" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="price">Giá sản phẩm <span class="text-danger">(*)</span></label>
                                <input type="number" name="price" id="price" placeholder="500.000 đ" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="sale">Giảm giá</label>
                                <input type="text" name="sale" value="0" min="0" max="100" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Danh mục món</label>
                                <select name="category_id" class="form-control">
                                    <?php while($row = $categories->fetch_assoc()) {?>
                                        <option value="<?=$row['id']?>"><?=$row['name']?></option>
                                    <?php }; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="avatar">Hình ảnh</label>
                                <input type="file" class="form-control-file" name="avatar">
                            </div>
                            <p>
                                <a href="/admin/danhmuc/home" class="btn btn-danger">Trở lại</a>
                                <input type="submit" name="btn" value="Thêm" class="btn btn-success">
                            </p>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>
<?php include 'templates/admin/layout/footer.php'; ?>