<?php include 'templates/admin/layout/header.php'; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include 'templates/admin/layout/nav.php'; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row mt-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin/home/danhmuc">Home</a></li>
                                <li class="breadcrumb-item"><a href="/admin/food/home">Món ăn</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <form class="form-group col-5" action="/admin/food/update" method="post" enctype="multipart/form-data">
                        <?php while($row = $food->fetch_assoc()) {?>
                            <input type="hidden" name="id" value="<?=$row['id']?>">
                            <div class="form-group">
                                <label for="name">Tên món ăn <span class="text-danger">(*)</span></label>
                                <input type="text" name="name" value="<?=$row['name']?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="price">Giá sản phẩm <span class="text-danger">(*)</span></label>
                                <input type="number" name="price" id="price" value="<?=$row['price']?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="sale">Giảm giá</label>
                                <input type="text" name="sale" value="<?=$row['sale']?>" min="0" max="100" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Danh mục món</label>
                                <select name="category_id" class="form-control">
                                    <?php while($row1 = $categories->fetch_assoc()) {
                                        if($row1['id'] == $row['category_id']){
                                            echo '<option value="'.$row1['id'].'" selected="selected">'.$row1['name'].'</option>';
                                        }else{
                                            echo '<option value="'.$row1['id'].'">'.$row1['name'].'</option>';
                                        }
                                    }; ?>
                                </select>
                            </div>
                            <?php }; ?>
                            <div class="form-group">
                                <label for="avatar">Hình ảnh</label>
                                <input type="file" class="form-control-file" name="avatar">
                            </div>
                            <p>
                                <a href="/admin/food/home" class="btn btn-danger">Trở lại</a>
                                <input type="submit" name="btn" value="Cập nhật" class="btn btn-success">
                            </p>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>
<?php include 'templates/admin/layout/footer.php'; ?>