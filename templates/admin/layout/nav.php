<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <div class="sb-sidenav-menu-heading">Home</div>
            <a class="nav-link" href="/admin/home/index">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Dashboard</a>
            <div class="sb-sidenav-menu-heading">Hiển thị</div>
            <a class="nav-link" href="/admin/danhmuc/home">
                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                Danh mục
            </a>
            <a class="nav-link" href="/admin/food/home" >
                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                Món ăn
            </a>
            <a class="nav-link" href="/admin/new/home" >
                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
               Bài viết
            </a>
            <div class="sb-sidenav-menu-heading">Quản lý</div>
            <a class="nav-link" href="charts.html">
                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                Đơn hàng</a>
            <a class="nav-link" href="tables.html">
                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                Người dùng</a>
        </div>
    </div>
</nav>