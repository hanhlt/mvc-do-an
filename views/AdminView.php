<?php
    class AdminView
    {
        public function home()
        {
            include 'templates/admin/index.php';
        }

        public function danhmuc()
        {
            include 'templates/admin/danhmuc/index.php';
        }
        public function add()
        {
            include 'templates/admin/danhmuc/add.php';
        }
    }