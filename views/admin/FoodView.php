<?php
    class FoodView
    {
        public function home($foods)
        {
            include 'templates/admin/monan/index.php';
        }

        public function add($categories)
        {
            include 'templates/admin/monan/add.php';
        }

        public function edit($food, $categories)
        {
            include 'templates/admin/monan/edit.php';
        }
    }
