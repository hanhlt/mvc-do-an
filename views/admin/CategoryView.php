<?php
    class CategoryView
    {
        public function home($data)
        {
            include 'templates/admin/danhmuc/index.php';
        }
        public function add()
        {
            include 'templates/admin/danhmuc/add.php';
        }
        public function edit($data)
        {
            include 'templates/admin/danhmuc/edit.php';
        }
    }
