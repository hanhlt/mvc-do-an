<?php
    class NewView
    {
        public function home()
        {
            include 'templates/admin/baiviet/index.php';
        }

        public function add()
        {
            include 'templates/admin/baiviet/add.php';
        }

        public function edit($data)
        {
            include 'templates/admin/baiviet/edit.php';
        }
    }
